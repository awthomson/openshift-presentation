# OpenShift
## Introduction to Red Hat OpenShift Container Platform

---

## Docker
* Uses Linux kernel functionality to isolate it's process from rest of OS
  * docker = namespaces + cgroups + overlayfs
* Emerged 2013, but follows some patterns that have been around a long time
* Like VMs but not like VMs

--

## Docker - Application density

![Docker Vs. VMs](resources/DockerVsVMs-Docker Vs VMs.png)

--

## Docker - Layers

![Layers](resources/DockerVsVMs-Layers.png)

--

## Terminology

* **Images**: A prebuilt software bundle, including all dependencies
* **Containers**: A running instance of an image
* **Registry**: A repository where docker images are stored
* **Dockerfile**: The set of instructions used to create an image

--

## Image Flow
<img src="resources/DockerVsVMs-Image Flow.png" data-canonical-src="resources/DockerVsVMs-Image Flow.png" width="500" height="500" />

--

## Docker - Benefits

* Application density
* Helps align to good DevOps practices
  * Environment consistency
  * Immutable 
  * Cattle vs. pets

--


## Problems Created

* Managing applications everywhere
* Networking, routing
* Scaling
* Need for a registry
* Access control

---

# Kubernetes

--

## What is K8s

* Free software with origins in Google
* First release in 2014

--

## Problems Solved

* Profileration of applications manageable
* Networking and Routing managed

--

## Problems Created

* Very steep learning curve
* Reliance on command-line, access to servers
* Access control
* Still leaves problem of how to turn code into a running container
* Data persistance
* Scaling

---

# OpenShift

--

## About Openshift

* Red Hat product, heavily reliant on underlying k8s
* Multiple versions:
  * __OKD__: Free, upstream version
  * __OCP__: Enterprise, on-prem version
  * __OpenShift Dedicated__: RH managed private version of OCP
  * __RHOO__: Public cloud hosted PaaS version

--

## Benefits

* Helps solve some of the gaps left by k8s
* Address the learning curve (a bit) by using a GUI
* Adds in some helper bits
  * Registry
  * Logging & metrics
  * Authn/Authz
  * Some template applications

--

## Design

<img src="resources/DockerVsVMs-OS Design.png" data-canonical-src="resources/DockerVsVMs-OS Design.png" width="700" height="500" />

--

## Tour of Console

--

## Terminology

* **Pods**: One or more containers deployerd together on a host
* **Build Config**: Instructions on how to build an Image
* **Deployment Config**: Config describing the running state of the pods
* **Services**: Ports and IPs assigned to a pod
* **Routes**: URL/Path assigned to a service
* **PVC**: Shared disk-space

--

## Learn More

* Transpower's Confluence (ES > Learning > DevOps)
* Free Account at RHOO