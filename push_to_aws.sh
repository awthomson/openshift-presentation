aws s3 cp index.html s3://openshift.presentation.athomson.co.nz
aws s3 cp custom.css s3://openshift.presentation.athomson.co.nz
aws s3 cp Presentation.md s3://openshift.presentation.athomson.co.nz
aws s3 cp --recursive css s3://openshift.presentation.athomson.co.nz/css
aws s3 cp --recursive js s3://openshift.presentation.athomson.co.nz/js
aws s3 cp --recursive resources s3://openshift.presentation.athomson.co.nz/resources
aws s3 cp --recursive lib s3://openshift.presentation.athomson.co.nz/lib
aws s3 cp --recursive plugin s3://openshift.presentation.athomson.co.nz/plugin